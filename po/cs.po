# Czech translation of evolution-mapi.
# Copyright (C) 2009, 2010, 2011 the author(s) of evolution-mapi.
# This file is distributed under the same license as the evolution-mapi package.
#
# Andre Klapper <ak-47@gmx.net>, 2009.
# Lucas Lommer <llommer@svn.gnome.org>, 2009.
# Jiri Eischmann <jiri@eischmann.cz>, 2009, 2010, 2011, 2012.
# Marek Černocký <marek@manet.cz>, 2010, 2011, 2012, 2016, 2017, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: evolution-mapi\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/evolution-mapi/issues\n"
"POT-Creation-Date: 2019-01-15 17:10+0000\n"
"PO-Revision-Date: 2019-01-16 17:30+0100\n"
"Last-Translator: Marek Černocký <marek@manet.cz>\n"
"Language-Team: čeština <gnome-cs-list@gnome.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../org.gnome.Evolution-mapi.metainfo.xml.in.h:1
msgid "Exchange MAPI"
msgstr "Exchange MAPI"

#: ../org.gnome.Evolution-mapi.metainfo.xml.in.h:2
#: ../src/camel/camel-mapi-provider.c:72
msgid "For accessing Microsoft Exchange 2007/OpenChange servers via MAPI"
msgstr "Přístup k serverům Microsoft Exchange 2007/OpenChange pomocí MAPI"

#: ../src/addressbook/e-book-backend-mapi.c:92
#: ../src/calendar/e-cal-backend-mapi.c:104
#: ../src/camel/camel-mapi-folder.c:1859 ../src/camel/camel-mapi-folder.c:1943
msgid "Unknown error"
msgstr "Neznámá chyba"

#: ../src/addressbook/e-book-backend-mapi.c:502
#: ../src/addressbook/e-book-backend-mapi.c:686
msgid "Failed to fetch GAL entries"
msgstr "Selhalo stahování záznamů GAL"

#: ../src/addressbook/e-book-backend-mapi.c:508
msgid "Failed to transfer contacts from a server"
msgstr "Selhal přenos kontaktů ze serveru"

#: ../src/addressbook/e-book-backend-mapi.c:693
#: ../src/calendar/e-cal-backend-mapi.c:1115
msgid "Failed to list items from a server"
msgstr "Selhalo vypsání položek ze serveru"

#: ../src/addressbook/e-book-backend-mapi.c:868
#: ../src/calendar/e-cal-backend-mapi.c:1342
msgid "Failed to modify item on a server"
msgstr "Selhala úprava složky na serveru"

#: ../src/addressbook/e-book-backend-mapi.c:868
#: ../src/calendar/e-cal-backend-mapi.c:1342
#: ../src/calendar/e-cal-backend-mapi.c:1636
msgid "Failed to create item on a server"
msgstr "Selhalo vytvoření složky na serveru"

#: ../src/addressbook/e-book-backend-mapi.c:929
#: ../src/calendar/e-cal-backend-mapi.c:1410
msgid "Failed to remove item from a server"
msgstr "Selhalo odstranění položky ze serveru"

#: ../src/calendar/e-cal-backend-mapi.c:891
msgid "Failed to transfer objects from a server"
msgstr "Selhal přenos objektů ze serveru"

#: ../src/calendar/e-cal-backend-mapi.c:1259
msgid ""
"Support for modifying single instances of a recurring appointment is not yet "
"implemented. No change was made to the appointment on the server."
msgstr ""
"Podpora pro úpravu jednotlivých výskytů opakujících se událostí není dosud "
"implementována. Změna události na serveru nebyla provedena."

#: ../src/calendar/e-cal-backend-mapi.c:1686
msgid "Failed to get Free/Busy data"
msgstr "Selhalo získání údajů o volných/obsazených termínech"

#: ../src/camel/camel-mapi-folder.c:776
#, c-format
msgid "Refreshing folder “%s”"
msgstr "Aktualizuje se složka „%s“"

#: ../src/camel/camel-mapi-folder.c:844
#, c-format
msgid "Downloading messages in folder “%s”"
msgstr "Stahují se zprávy ve složce „%s“"

#: ../src/camel/camel-mapi-folder.c:932 ../src/camel/camel-mapi-folder.c:1492
#, c-format
msgid "This message is not available in offline mode."
msgstr "Tato zpráva není k dispozici v režimu odpojení."

#: ../src/camel/camel-mapi-folder.c:942 ../src/camel/camel-mapi-folder.c:960
#, c-format
msgid "Fetching items failed: %s"
msgstr "Stahování položek selhalo: %s"

#: ../src/camel/camel-mapi-folder.c:947 ../src/camel/camel-mapi-folder.c:965
msgid "Fetching items failed"
msgstr "Stahování položek selhalo"

#: ../src/camel/camel-mapi-folder.c:1170
#, c-format
msgid "Cannot append message to folder “%s”"
msgstr "Nezdařilo se připojit zprávu ke složce „%s“"

#: ../src/camel/camel-mapi-folder.c:1179 ../src/camel/camel-mapi-folder.c:1209
#, c-format
msgid "Offline."
msgstr "Off-line."

#: ../src/camel/camel-mapi-folder.c:1293
#, c-format
msgid "Failed to empty Trash: %s"
msgstr "Selhalo vyprázdnění koše: %s"

#: ../src/camel/camel-mapi-folder.c:1299
msgid "Failed to empty Trash"
msgstr "Selhalo vyprázdnění koše"

#. Translators: The first %s is replaced with a message ID,
#. the second %s is replaced with a detailed error string
#: ../src/camel/camel-mapi-folder.c:1477
#, c-format
msgid "Cannot get message %s: %s"
msgstr "Nelze získat zprávu %s: %s"

#: ../src/camel/camel-mapi-folder.c:1478
msgid "No such message"
msgstr "Taková zpráva neexistuje"

#: ../src/camel/camel-mapi-folder.c:1503 ../src/camel/camel-mapi-folder.c:1537
#, c-format
msgid "Could not get message: %s"
msgstr "Nelze získat zprávu: %s"

#: ../src/camel/camel-mapi-folder.c:1509 ../src/camel/camel-mapi-folder.c:1544
#, c-format
msgid "Could not get message"
msgstr "Nelze získat zprávu"

#: ../src/camel/camel-mapi-folder.c:1920
msgid "Receive quota"
msgstr "Přijmout kvótu"

#: ../src/camel/camel-mapi-folder.c:1926
msgid "Send quota"
msgstr "Odeslat kvótu"

#: ../src/camel/camel-mapi-folder.c:1948
msgid "No quota information available"
msgstr "Informace o kvótě nejsou k dispozici"

#: ../src/camel/camel-mapi-folder.c:2055
#, c-format
msgid "Could not load summary for %s"
msgstr "Nelze načíst souhrn pro %s"

#: ../src/camel/camel-mapi-provider.c:43
msgid "Checking for new mail"
msgstr "Kontroluje se nová pošta"

#: ../src/camel/camel-mapi-provider.c:45
msgid "C_heck for new messages in all folders"
msgstr "_Kontrolovat nové zprávy ve všech složkách"

#: ../src/camel/camel-mapi-provider.c:47
msgid "Lis_ten for server change notifications"
msgstr "Nasloucha_t oznámením o změnách od serveru"

#: ../src/camel/camel-mapi-provider.c:51
msgid "Options"
msgstr "Volby"

#. i18n: copy from evolution:camel-imap-provider.c
#: ../src/camel/camel-mapi-provider.c:54
msgid "_Apply filters to new messages in Inbox on this server"
msgstr "_Použít filtry na nové zprávy ve složce Přijatá na tomto serveru"

#: ../src/camel/camel-mapi-provider.c:56
msgid "Check new messages for _Junk contents"
msgstr "_Hledat nevyžádanou poštu mezi novými zprávami"

#: ../src/camel/camel-mapi-provider.c:58
msgid "Only check for Junk messag_es in the Inbox folder"
msgstr "Hledat nevyžádanou poštu jen ve složce _Přijatá"

#: ../src/camel/camel-mapi-provider.c:60
msgid "Synchroni_ze remote mail locally in all folders"
msgstr "Synchroni_zovat vzdálenou poštu na místní počítač ve všech složkách"

#: ../src/camel/camel-mapi-provider.c:87
msgid "Password"
msgstr "Heslo"

#: ../src/camel/camel-mapi-provider.c:88
msgid ""
"This option will connect to the OpenChange server using a plaintext password."
msgstr ""
"Touto volbou se připojíte k serveru OpenChange pomocí hesla v podobě čistého "
"textu."

#: ../src/camel/camel-mapi-sasl-krb.c:26
msgid "Kerberos"
msgstr "Kerberos"

#: ../src/camel/camel-mapi-sasl-krb.c:28
msgid "This option will connect to the server using kerberos key."
msgstr "Touto volbou se připojíte k serveru pomocí klíče kerberos."

#: ../src/camel/camel-mapi-store.c:132 ../src/camel/camel-mapi-store.c:174
msgid "Cannot find folder in a local cache"
msgstr "Nelze najít složku v místní mezipaměti"

#: ../src/camel/camel-mapi-store.c:489 ../src/camel/camel-mapi-store.c:1182
msgid "Folder list is not available in offline mode"
msgstr "Seznam složek není k dispozici v režimu odpojení"

#: ../src/camel/camel-mapi-store.c:879
msgid "No public folder found"
msgstr "Nenalezena žádná veřejná složka"

#: ../src/camel/camel-mapi-store.c:879
msgid "No folder found"
msgstr "Nenalezena žádná složka"

#: ../src/camel/camel-mapi-store.c:1204 ../src/camel/camel-mapi-store.c:2272
#, c-format
msgid "Connecting to “%s”"
msgstr "Připojuje se k „%s“"

#: ../src/camel/camel-mapi-store.c:1263
msgid "Cannot create MAPI folders in offline mode"
msgstr "Nelze vytvořit složky MAPI v režimu odpojení"

#: ../src/camel/camel-mapi-store.c:1270
#, c-format
msgid "Cannot create new folder “%s”"
msgstr "Nezdařilo se vytvořit novou složku „%s“"

#: ../src/camel/camel-mapi-store.c:1279
#, c-format
msgid "Authentication failed"
msgstr "Ověření totožnosti selhalo"

#: ../src/camel/camel-mapi-store.c:1289
msgid "MAPI folders can be created only within mailbox of the logged in user"
msgstr ""
"Složky MAPI mohou být vytvořeny pouze v poštovní schránce přihlášeného "
"uživatele"

#: ../src/camel/camel-mapi-store.c:1302 ../src/camel/camel-mapi-store.c:1415
#, c-format
msgid "Cannot find folder “%s”"
msgstr "Nelze najít složku „%s“"

#: ../src/camel/camel-mapi-store.c:1357
#, c-format
msgid "Cannot create folder “%s”: %s"
msgstr "Nelze vytvořit složku „%s“: %s"

#: ../src/camel/camel-mapi-store.c:1363
#, c-format
msgid "Cannot create folder “%s”"
msgstr "Nelze vytvořit složku „%s“"

#: ../src/camel/camel-mapi-store.c:1393 ../src/camel/camel-mapi-store.c:1405
msgid "Cannot delete MAPI folders in offline mode"
msgstr "Nelze odstranit složky MAPI v režimu odpojení"

#: ../src/camel/camel-mapi-store.c:1461
#, c-format
msgid "Cannot remove folder “%s”: %s"
msgstr "Nelze odstranit složku „%s“: %s"

#: ../src/camel/camel-mapi-store.c:1469
#, c-format
msgid "Cannot remove folder “%s”"
msgstr "Nelze odstranit složku „%s“"

#: ../src/camel/camel-mapi-store.c:1499 ../src/camel/camel-mapi-store.c:1514
msgid "Cannot rename MAPI folders in offline mode"
msgstr "Nelze přejmenovat složky MAPI v režimu odpojení"

#. Translators: “%s” is current name of the folder
#: ../src/camel/camel-mapi-store.c:1525
#, c-format
msgid "Cannot rename MAPI folder “%s”. Folder does not exist"
msgstr "Nelze přejmenovat složku MAPI „%s“. Složka neexistuje"

#. Translators: “%s to %s” is current name of the folder  and
#. new name of the folder.
#: ../src/camel/camel-mapi-store.c:1536
#, c-format
msgid "Cannot rename MAPI default folder “%s” to “%s”"
msgstr "Nelze přejmenovat výchozí složku MAPI „%s“ na „%s“"

#. Translators: “%s to %s” is current name of the folder and new name of the folder.
#: ../src/camel/camel-mapi-store.c:1562 ../src/camel/camel-mapi-store.c:1605
#: ../src/camel/camel-mapi-store.c:1685
#, c-format
msgid "Cannot rename MAPI folder “%s” to “%s”"
msgstr "Nelze přejmenovat složku MAPI „%s“ na „%s“"

#. Translators: “%s to %s” is current name of the folder and new name of the folder.
#. The last “%s” is a detailed error message.
#: ../src/camel/camel-mapi-store.c:1597 ../src/camel/camel-mapi-store.c:1678
#, c-format
msgid "Cannot rename MAPI folder “%s” to “%s”: %s"
msgstr "Nelze přejmenovat složku MAPI „%s“ na „%s“: %s"

#: ../src/camel/camel-mapi-store.c:1775
msgid "Cannot subscribe MAPI folders in offline mode"
msgstr "Nelze se přihlašovat ke složkám MAPI v režimu odpojení"

#: ../src/camel/camel-mapi-store.c:1792
#, c-format
msgid "Folder “%s” not found"
msgstr "Složka „%s“ nebyla nalezena"

#: ../src/camel/camel-mapi-store.c:1917
msgid "Cannot unsubscribe MAPI folders in offline mode"
msgstr "Nelze se odhlašovat od složek MAPI v režimu odpojení"

#. Translators: The %s is replaced with a server's host name
#: ../src/camel/camel-mapi-store.c:2219 ../src/camel/camel-mapi-transport.c:194
#, c-format
msgid "Exchange MAPI server %s"
msgstr "Server Exchange MAPI %s"

#. To translators : Example string : Exchange MAPI service for
#. _username_ on _server host name__
#. Translators: The first %s is replaced with a user name, the second with a server's host name
#: ../src/camel/camel-mapi-store.c:2223 ../src/camel/camel-mapi-transport.c:197
#, c-format
msgid "Exchange MAPI service for %s on %s"
msgstr "Služba Exchange MAPI pro %s na %s"

#: ../src/camel/camel-mapi-store.c:2254
msgid "Cannot connect to MAPI store in offline mode"
msgstr "Nelze připojit úložiště MAPI v režimu odpojení"

#: ../src/camel/camel-mapi-store.c:2307
#, c-format
msgid "Mailbox “%s” is full, no new messages will be received or sent."
msgstr ""
"Poštovní schránka „%s“ je plná, žádné nové zprávy nebudou přijímány nebo "
"odesílány."

#: ../src/camel/camel-mapi-store.c:2309
#, c-format
msgid ""
"Mailbox “%s” is near its size limit, message send will be disabled soon."
msgstr ""
"Poštovní schránka „%s“ je blízko limitu své velikosti, odesílání zpráv bude "
"brzo zakázáno."

#: ../src/camel/camel-mapi-store.c:2313
#, c-format
msgid "Mailbox “%s” is full, no new messages will be received."
msgstr "Poštovní schránka „%s“ je plná, žádné nové zprávy nebudou přijímány."

#: ../src/camel/camel-mapi-store.c:2315
#, c-format
msgid "Mailbox “%s” is near its size limit."
msgstr "Poštovní schránka „%s“ je blízko limitu své velikosti."

#: ../src/camel/camel-mapi-store.c:2733
#, c-format
msgid "Cannot add folder “%s”, failed to add to store’s summary"
msgstr ""
"Nezdařilo se přidat složku „%s“, selhalo přidání do souhrnného úložiště"

#: ../src/camel/camel-mapi-store.c:3016
msgid "Authentication password not available"
msgstr "Autentizační heslo není k dispozici"

#: ../src/camel/camel-mapi-store.c:3052 ../src/camel/camel-mapi-store.c:3468
msgid "Updating foreign folders"
msgstr "Aktualizují se vzdálené složky"

#. Translators: the first '%s' is replaced with a generic error message,
#. the second '%s' is replaced with additional error information.
#: ../src/camel/camel-mapi-store.c:3075 ../src/collection/e-mapi-backend.c:899
#: ../src/configuration/e-mail-config-mapi-backend.c:406
#: ../src/configuration/e-mapi-config-utils.c:341
#, c-format
msgctxt "gssapi_error"
msgid "%s (%s)"
msgstr "%s (%s)"

#: ../src/camel/camel-mapi-store.h:53
msgid "Favorites"
msgstr "Oblíbené"

#: ../src/camel/camel-mapi-store.h:54
msgid "Foreign folders"
msgstr "Vzdálené složky"

#: ../src/camel/camel-mapi-transport.c:145
#: ../src/camel/camel-mapi-transport.c:167
#, c-format
msgid "Could not send message."
msgstr "Nelze odeslat zprávu."

#: ../src/camel/camel-mapi-transport.c:162
#, c-format
msgid "Could not send message: %s"
msgstr "Nelze odeslat zprávu: %s"

#: ../src/collection/e-mapi-backend.c:76
msgid "Cannot connect, no credentials provided"
msgstr "Nelze připojit, nejsou poskytnuta žádná pověření"

#: ../src/collection/e-mapi-backend.c:373
msgid "Global Address List"
msgstr "Globální seznam adres"

#: ../src/collection/e-mapi-backend.c:728
#: ../src/collection/e-mapi-backend.c:825
#, c-format
msgid "Data source “%s” does not represent a MAPI folder"
msgstr "Zdroj dat „%s“ se ve složce MAPI nenachází"

#: ../src/configuration/e-book-config-mapigal.c:59
msgid "Allow _partial search results"
msgstr "_Povolit částečné výsledky hledání"

#: ../src/configuration/e-mail-config-mapi-backend.c:123
msgid "Select username"
msgstr "Vyberte jméno uživatele"

#: ../src/configuration/e-mail-config-mapi-backend.c:133
msgid "Full name"
msgstr "Celé jméno"

#: ../src/configuration/e-mail-config-mapi-backend.c:138
msgid "Username"
msgstr "Jméno uživatele"

#: ../src/configuration/e-mail-config-mapi-backend.c:165
msgid ""
"There are more users with similar user name on a server.\n"
"Please select that you would like to use from the below list."
msgstr ""
"Na serveru existuje více uživatelů s podobným uživatelským jménem.\n"
"Vyberte prosím v následujícím seznamu, které jméno chcete použít."

#: ../src/configuration/e-mail-config-mapi-backend.c:361
msgid "Authentication finished successfully."
msgstr "Ověření totožnosti úspěšně dokončeno."

#: ../src/configuration/e-mail-config-mapi-backend.c:363
#: ../src/configuration/e-mail-config-mapi-backend.c:501
msgid "Authentication failed."
msgstr "Ověření totožnosti selhalo."

#: ../src/configuration/e-mail-config-mapi-backend.c:444
msgid "Cannot authenticate MAPI accounts in offline mode"
msgstr "Nelze ověřit totožnost u účtů MAPI v režimu odpojení"

#: ../src/configuration/e-mail-config-mapi-backend.c:473
msgid ""
"Server, username and domain name cannot be empty. Please fill them with "
"correct values."
msgstr ""
"Server, jméno uživatele a název domény nemohou být prázdné. Vyplňte do nich "
"prosím správné údaje."

#: ../src/configuration/e-mail-config-mapi-backend.c:476
msgid ""
"Realm name cannot be empty when kerberos is selected. Please fill them with "
"correct values."
msgstr ""
"Pokud je vybrán kerberos, nemůže být název sféry prázdný. Vyplňte prosím "
"správné údaje."

#: ../src/configuration/e-mail-config-mapi-backend.c:495
msgid "Connecting to the server, please wait…"
msgstr "Připojuje se k serveru, čekejte prosím…"

#: ../src/configuration/e-mail-config-mapi-backend.c:697
msgid "Configuration"
msgstr "Nastavení"

#: ../src/configuration/e-mail-config-mapi-backend.c:706
msgid "_Server:"
msgstr "_Server:"

#: ../src/configuration/e-mail-config-mapi-backend.c:723
msgid "User_name:"
msgstr "Jmé_no uživatele:"

#: ../src/configuration/e-mail-config-mapi-backend.c:748
msgid "_Domain name:"
msgstr "Název _domény:"

#: ../src/configuration/e-mail-config-mapi-backend.c:761
msgid "_Authenticate"
msgstr "_Ověřit totožnost"

#: ../src/configuration/e-mail-config-mapi-backend.c:769
msgid "_Use secure connection"
msgstr "Po_užít zabezpečené spojení"

#: ../src/configuration/e-mail-config-mapi-backend.c:784
msgid "_Kerberos authentication"
msgstr "Ověření totožnosti _Kerberos"

#: ../src/configuration/e-mail-config-mapi-backend.c:796
msgid "_Realm name:"
msgstr "Název sfé_ry:"

#: ../src/configuration/e-mail-config-mapi-page.c:189
#: ../src/configuration/e-mail-config-mapi-page.c:252
msgid "MAPI Settings"
msgstr "Nastavení MAPI"

#: ../src/configuration/e-mail-config-mapi-page.c:195
msgid "View the size of all Exchange folders"
msgstr "Zobrazit velikost všech složek Exchange"

#: ../src/configuration/e-mail-config-mapi-page.c:199
msgid "Folder _Size"
msgstr "Velikost _složky"

#: ../src/configuration/e-mapi-config-utils.c:432
msgid "Folder"
msgstr "Složka"

#: ../src/configuration/e-mapi-config-utils.c:437
msgid "Size"
msgstr "Velikost"

#: ../src/configuration/e-mapi-config-utils.c:460
#: ../src/configuration/e-mapi-config-utils.c:464
msgid "Unable to retrieve folder size information"
msgstr "Nelze získat informace o velikosti složky"

#: ../src/configuration/e-mapi-config-utils.c:531
msgid "Folder Size"
msgstr "Velikost složky"

#: ../src/configuration/e-mapi-config-utils.c:545
msgid "Fetching folder list…"
msgstr "Probíhá stahování seznamu složek…"

#: ../src/configuration/e-mapi-config-utils.c:612
#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:752
msgid "Subscribe to folder of other MAPI user…"
msgstr "Číst složky jiného uživatele MAPI…"

#: ../src/configuration/e-mapi-config-utils.c:810
#, c-format
msgid "Cannot edit permissions of folder “%s”, choose other folder."
msgstr "Nelze upravovat oprávnění složky „%s“, zvolte jinou složku."

#: ../src/configuration/e-mapi-config-utils.c:873
msgid "Folder size…"
msgstr "Velikost složky…"

#: ../src/configuration/e-mapi-config-utils.c:880
msgid "Subscribe to folder of other user…"
msgstr "Číst složky jiného uživatele…"

#: ../src/configuration/e-mapi-config-utils.c:889
#: ../src/configuration/e-mapi-config-utils.c:1203
#: ../src/configuration/e-mapi-config-utils.c:1240
#: ../src/configuration/e-mapi-config-utils.c:1277
#: ../src/configuration/e-mapi-config-utils.c:1314
msgid "Permissions…"
msgstr "Oprávnění…"

#: ../src/configuration/e-mapi-config-utils.c:891
msgid "Edit MAPI folder permissions"
msgstr "Upravit oprávnění složky MAPI"

#: ../src/configuration/e-mapi-config-utils.c:1205
msgid "Edit MAPI calendar permissions"
msgstr "Upravit oprávnění kalendáře MAPI"

#: ../src/configuration/e-mapi-config-utils.c:1242
msgid "Edit MAPI tasks permissions"
msgstr "Upravit oprávnění úkolů MAPI"

#: ../src/configuration/e-mapi-config-utils.c:1279
msgid "Edit MAPI memos permissions"
msgstr "Upravit oprávnění poznámek MAPI"

#: ../src/configuration/e-mapi-config-utils.c:1316
msgid "Edit MAPI contacts permissions"
msgstr "Upravit oprávnění kontaktů MAPI"

#: ../src/configuration/e-mapi-config-utils.c:1496
msgid "Personal Folders"
msgstr "Osobní složky"

#: ../src/configuration/e-mapi-config-utils.c:1727
msgid "Searching remote MAPI folder structure, please wait…"
msgstr "Prohledávání struktury vzdálených složek MAPI, prosím čekejte…"

#: ../src/configuration/e-mapi-config-utils.c:1774
msgid "Lis_ten for server notifications"
msgstr "Nasloucha_t oznámením od serveru"

#: ../src/configuration/e-mapi-config-utils.c:1795
msgid "Cannot create MAPI calendar in offline mode"
msgstr "Nelze vytvořit kalendář MAPI v režimu odpojení"

#: ../src/configuration/e-mapi-config-utils.c:1798
msgid "Cannot create MAPI task list in offline mode"
msgstr "Nelze vytvořit seznam úkolů MAPI v režimu odpojení"

#: ../src/configuration/e-mapi-config-utils.c:1801
msgid "Cannot create MAPI memo list in offline mode"
msgstr "Nelze vytvořit seznam poznámek MAPI v režimu odpojení"

#: ../src/configuration/e-mapi-config-utils.c:1804
msgid "Cannot create MAPI address book in offline mode"
msgstr "Nelze vytvořit adresář MAPI v režimu odpojení"

#: ../src/configuration/e-mapi-config-utils.c:1809
msgid "Cannot create MAPI source in offline mode"
msgstr "Nelze vytvořit zdroj MAPI v režimu odpojení"

#: ../src/configuration/e-mapi-config-utils.c:1829
msgid "_Location:"
msgstr "_Místo:"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:87
msgctxt "PermissionsLevel"
msgid "None"
msgstr "Žádné"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:88
msgctxt "PermissionsLevel"
msgid "Owner"
msgstr "Vlastník"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:98
msgctxt "PermissionsLevel"
msgid "Publishing Editor"
msgstr "Šéfredaktor - vydavatel"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:107
msgctxt "PermissionsLevel"
msgid "Editor"
msgstr "Editor"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:115
msgctxt "PermissionsLevel"
msgid "Publishing Author"
msgstr "Redaktor - vydavatel"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:122
msgctxt "PermissionsLevel"
msgid "Author"
msgstr "Autor"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:128
msgctxt "PermissionsLevel"
msgid "Nonediting Author"
msgstr "Redaktor – konzultant"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:133
msgctxt "PermissionsLevel"
msgid "Reviewer"
msgstr "Recenzent"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:136
msgctxt "PermissionsLevel"
msgid "Contributor"
msgstr "Přispěvatel"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:139
msgctxt "PermissionsLevel"
msgid "Custom"
msgstr "Vlastní"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:272
msgid "Writing folder permissions, please wait…"
msgstr "Zapisuje se oprávnění ke složce, prosím čekejte…"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:782
#: ../src/configuration/e-mapi-search-gal-user.c:525
msgctxt "User"
msgid "Anonymous"
msgstr "Anonymní"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:784
#: ../src/configuration/e-mapi-search-gal-user.c:522
msgctxt "User"
msgid "Default"
msgstr "Výchozí"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:786
msgctxt "User"
msgid "Unknown"
msgstr "Neznámý"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:880
#: ../src/configuration/e-mapi-search-gal-user.c:600
msgid "Name"
msgstr "Jméno"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:886
msgid "Permission level"
msgstr "Úroveň oprávnění"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:939
msgid "Edit MAPI folder permissions…"
msgstr "Upravit oprávnění složky MAPI…"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:964
#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:773
msgid "Account:"
msgstr "Účet:"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:990
msgid "Folder name:"
msgstr "Název složky:"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1011
msgid "Folder ID:"
msgstr "ID složky:"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1070
msgid "Permissions"
msgstr "Oprávnění"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1091
msgid "Permi_ssion level:"
msgstr "Úro_veň oprávnění:"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1119
msgctxt "Permissions"
msgid "Read"
msgstr "Čtení"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1130
#: ../src/configuration/e-mapi-edit-folder-permissions.c:1193
msgctxt "Permissions"
msgid "None"
msgstr "Žádná"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1135
msgctxt "Permissions"
msgid "Full Details"
msgstr "Úplné podrobnosti"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1140
msgctxt "Permissions"
msgid "Simple Free/Busy"
msgstr "Zjednodušeně volno/obsazeno"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1144
msgctxt "Permissions"
msgid "Detailed Free/Busy"
msgstr "Podrobně volno/obsazeno"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1151
msgctxt "Permissions"
msgid "Write"
msgstr "Zápis"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1162
msgctxt "Permissions"
msgid "Create items"
msgstr "Vytvořit položky"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1166
msgctxt "Permissions"
msgid "Create subfolders"
msgstr "Vytvořit podsložky"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1170
msgctxt "Permissions"
msgid "Edit own"
msgstr "Upravit vlastní"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1174
msgctxt "Permissions"
msgid "Edit all"
msgstr "Upravit všechny"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1182
msgctxt "Permissions"
msgid "Delete items"
msgstr "Odstranit položky"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1198
msgctxt "Permissions"
msgid "Own"
msgstr "Vlastní"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1203
msgctxt "Permissions"
msgid "All"
msgstr "Všechna"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1209
msgctxt "Permissions"
msgid "Other"
msgstr "Ostatní"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1220
msgctxt "Permissions"
msgid "Folder owner"
msgstr "Vlastník složky"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1224
msgctxt "Permissions"
msgid "Folder contact"
msgstr "Kontakt složky"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1228
msgctxt "Permissions"
msgid "Folder visible"
msgstr "Viditelná složka"

#: ../src/configuration/e-mapi-edit-folder-permissions.c:1283
msgid "Reading folder permissions, please wait…"
msgstr "Čtou se oprávnění ke složce, prosím čekejte…"

#: ../src/configuration/e-mapi-search-gal-user.c:221
msgid "No users found"
msgstr "Žádní uživatelé nenalezeni"

#: ../src/configuration/e-mapi-search-gal-user.c:224
#, c-format
msgid "Found one user"
msgid_plural "Found %d users"
msgstr[0] "Nalezen jeden uživatel"
msgstr[1] "Nalezeni %d uživatelé"
msgstr[2] "Nalezelo %d uživatelů"

#: ../src/configuration/e-mapi-search-gal-user.c:229
#, c-format
msgid "Found %d user, but showing only first %d"
msgid_plural "Found %d users, but showing only first %d"
msgstr[0] "Nalezen %d uživatel, ale zobrazeno je pouze prvních %d"
msgstr[1] "Nalezeni %d uživatelé, ale zobrazeno je pouze prvních %d"
msgstr[2] "Nalezeno %d uživatelů, ale zobrazeno je pouze prvních %d"

#: ../src/configuration/e-mapi-search-gal-user.c:517
#: ../src/configuration/e-mapi-search-gal-user.c:710
msgid "Search for a user"
msgstr "Hledat uživatele"

#: ../src/configuration/e-mapi-search-gal-user.c:533
msgid "Searching…"
msgstr "Hledá se…"

#: ../src/configuration/e-mapi-search-gal-user.c:606
msgid "E-mail"
msgstr "E-mail"

#: ../src/configuration/e-mapi-search-gal-user.c:647
msgid "Choose MAPI user…"
msgstr "Zvolit uživatele MAPI…"

#: ../src/configuration/e-mapi-search-gal-user.c:670
msgid "_Search:"
msgstr "_Hledat:"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:95
#, c-format
msgid "Cannot add folder, folder already exists as “%s”"
msgstr "Nelze přidat složku, složka již existuje jako „%s“"

#. Translators: The '%s' is replaced with user name, to whom the foreign mailbox belongs.
#. Example result: "Mailbox — John Smith"
#.
#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:120
#, c-format
msgctxt "ForeignFolder"
msgid "Mailbox — %s"
msgstr "Poštovní schránka – %s"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:150
#, c-format
msgid "Cannot add folder, failed to add to store’s summary"
msgstr "Nelze přidat složku, selhalo přidání do souhrnného úložiště"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:373
#, c-format
msgid ""
"Folder “%s” not found. Either it does not exist or you do not have "
"permission to access it."
msgstr ""
"Složka „%s“ nebyla nalezena. Buď neexistuje, nebo k ní nemáte oprávnění pro "
"přístup."

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:408
msgid "Cannot add folder, cannot determine folder’s type"
msgstr "Nelze přidat složku, nelze určit typ složky"

#. Translators: This is used to name foreign folder.
#. The first '%s' is replaced with user name to whom the folder belongs,
#. the second '%s' is replaced with folder name.
#. Example result: "John Smith — Calendar"
#.
#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:449
#, c-format
msgctxt "ForeignFolder"
msgid "%s — %s"
msgstr "%s — %s"

#. convert well-known names to their non-localized form
#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:568
#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:846
msgid "Inbox"
msgstr "Doručená pošta"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:570
#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:847
msgid "Contacts"
msgstr "Kontakty"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:572
#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:848
msgid "Calendar"
msgstr "Kalendář"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:574
#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:849
msgid "Memos"
msgstr "Poznámky"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:576
#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:850
msgid "Tasks"
msgstr "Úkoly"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:590
#, c-format
msgid "Testing availability of folder “%s” of user “%s”, please wait…"
msgstr "Testuje se dostupnost složky „%s“ uživatele „%s“, čekejte prosím…"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:628
msgid "Cannot search for user when the account is offline"
msgstr "Nelze hledat uživatele, když je účet v režimu odpojení"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:796
msgid "User"
msgstr "Uživatel"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:803
msgid "_User:"
msgstr "_Uživatel:"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:816
msgid "C_hoose…"
msgstr "V_ybrat…"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:831
msgid "_Folder name:"
msgstr "Název _složky:"

#: ../src/configuration/e-mapi-subscribe-foreign-folder.c:859
msgid "Include _subfolders"
msgstr "Včetně pod_složek"

#. Translators: This is a meeting response prefix which will be shown in a message Subject
#: ../src/libexchangemapi/e-mapi-cal-utils.c:2065
msgctxt "MeetingResp"
msgid "Accepted:"
msgstr "Schválené:"

#. Translators: This is a meeting response prefix which will be shown in a message Subject
#: ../src/libexchangemapi/e-mapi-cal-utils.c:2070
msgctxt "MeetingResp"
msgid "Tentative:"
msgstr "Předběžné:"

#. Translators: This is a meeting response prefix which will be shown in a message Subject
#: ../src/libexchangemapi/e-mapi-cal-utils.c:2075
msgctxt "MeetingResp"
msgid "Declined:"
msgstr "Zamítnuté:"

#: ../src/libexchangemapi/e-mapi-connection.c:139
msgid "Failed to login into the server"
msgstr "Selhalo přihlášení k serveru."

#: ../src/libexchangemapi/e-mapi-connection.c:140
msgid "Cannot create more sessions, session limit was reached"
msgstr "Nelze vytvořit více sezení, byl dosažen limit sezení"

#: ../src/libexchangemapi/e-mapi-connection.c:141
msgid "User cancelled operation"
msgstr "Uživatel zrušil operaci"

#: ../src/libexchangemapi/e-mapi-connection.c:142
msgid "Unable to abort"
msgstr "Nelze přerušit"

#: ../src/libexchangemapi/e-mapi-connection.c:143
msgid "Network error"
msgstr "Síťová chyba"

#: ../src/libexchangemapi/e-mapi-connection.c:144
msgid "Disk error"
msgstr "Disková chyba"

#: ../src/libexchangemapi/e-mapi-connection.c:145
msgid "Password change required"
msgstr "Požadována změna hesla"

#: ../src/libexchangemapi/e-mapi-connection.c:146
msgid "Password expired"
msgstr "Heslo vypršelo"

#: ../src/libexchangemapi/e-mapi-connection.c:147
msgid "Invalid workstation account"
msgstr "Neplatný účet pracovní stanice"

#: ../src/libexchangemapi/e-mapi-connection.c:148
msgid "Invalid access time"
msgstr "Neplatný čas přístupu"

#: ../src/libexchangemapi/e-mapi-connection.c:149
msgid "Account is disabled"
msgstr "Účet je zakázaný"

#: ../src/libexchangemapi/e-mapi-connection.c:150
msgid "End of session"
msgstr "Konec sezení"

#: ../src/libexchangemapi/e-mapi-connection.c:151
msgid "MAPI is not initialized or connected"
msgstr "MAPI není inicializováno nebo připojeno"

#: ../src/libexchangemapi/e-mapi-connection.c:152
msgid "Permission denied"
msgstr "Přístup odepřen"

#: ../src/libexchangemapi/e-mapi-connection.c:153
msgid "Mailbox quota exceeded"
msgstr "Kvóta poštovní schránky překročena"

#: ../src/libexchangemapi/e-mapi-connection.c:161
#, c-format
msgid "MAPI error %s (0x%x) occurred"
msgstr "Vyskytla se chyba MAPI %s (0x%x)"

#. Translators: The first '%s' is replaced with an error context,
#. aka where the error occurred, the second '%s' is replaced with
#. the error message.
#: ../src/libexchangemapi/e-mapi-connection.c:179
#, c-format
msgctxt "EXCHANGEMAPI_ERROR"
msgid "%s: %s"
msgstr "%s: %s"

#: ../src/libexchangemapi/e-mapi-connection.c:744
#, c-format
msgid "Server “%s” cannot be reached"
msgstr "Server „%s“ je nedosažitelný"

#: ../src/libexchangemapi/e-mapi-connection.c:872
#, c-format
msgid "Folder name “%s” is not a known default folder name, nor folder ID."
msgstr "Název složky „%s“ není ani obvyklý výchozí název složky ani ID složky."

#: ../src/libexchangemapi/e-mapi-connection.c:1203
#, c-format
msgid "Failed to open store for user “%s”"
msgstr "Selhalo otevření úložiště pro uživatele „%s“"

#: ../src/libexchangemapi/e-mapi-connection.c:1211
#, c-format
msgid "Folder of user “%s” not found"
msgstr "Složka uživatele „%s“ nebyla nalezena"

#. Translators: %s is replaced with an email address which was found ambiguous on a remote server
#: ../src/libexchangemapi/e-mapi-connection.c:3863
#, c-format
msgid "Recipient “%s” is ambiguous"
msgstr "Příjemce „%s“ je nejednoznačný"

#: ../src/libexchangemapi/e-mapi-connection.c:4955
#, c-format
msgid ""
"Search result exceeded allowed size limit. Use more specific search term, "
"please"
msgstr ""
"Výsledky hledání překročily povolený limit. Použijte prosím konkrétnější "
"termín."

#: ../src/libexchangemapi/e-mapi-connection.c:6577
msgid "All Public Folders"
msgstr "Všechny veřejné složky"

#: ../src/libexchangemapi/e-mapi-connection.c:6885
#, c-format
msgid "User name “%s” is ambiguous"
msgstr "Uživatelské jméno „%s“ je nejednoznačné"

#: ../src/libexchangemapi/e-mapi-connection.c:6888
#, c-format
msgid "User name “%s” not found"
msgstr "Uživatelské jméno „%s“ nebylo nalezeno"

#: ../src/libexchangemapi/e-mapi-folder.c:330
msgid "Cannot add folder, unsupported folder type"
msgstr "Nelze přidat složku, nepodporovaný typ složky"

#: ../src/libexchangemapi/e-mapi-folder.c:333
msgid "Cannot add folder, master source not found"
msgstr "Nelze přidat složku, hlavní zdroj nebyl nalezen"

#: ../src/libexchangemapi/e-mapi-utils.c:854
#, c-format
msgid ""
"Cannot ask for Kerberos ticket. Obtain the ticket manually, like on command "
"line with “kinit” or open “Online Accounts” in “Settings” and add the "
"Kerberos account there. Reported error was: %s"
msgstr ""
"Nezdařilo se požádat o tiket Kerberos. Získejte tiket ručně, například z "
"příkazové řádky pomocí „kinit“, nebo otevřením „Účtů on-line“ v "
"„Nastaveních“, ve kterých přidejte účet Kerberos. Nahlášená chyby byla: %s"
